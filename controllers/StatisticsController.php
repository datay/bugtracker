<?php

class StatisticsController 
{
	public static function showStat()
	{
		$weekly_trend = array(0 => array('Number of Bugs', 'Week'));
		foreach(IrisError::getWeeklyFrequency() as $week) {
			$weekly_trend[] = array($week['entry_count'], (int) $week['date_part']);
		}

		$bug_stat[] = array('Component', 'Total Number of Bags');
		foreach(IrisError::getBugsStatByComponent() as $component) {
			$bug_stat[] = array($component['component'], (int) $component['total_count']);
		}
		set('weekly_trend', $weekly_trend);
		set('bug_stat', $bug_stat);
		set('winner_loser', IrisError::getWinnerLoserData());
		return html('statistics/show_stat.php');	
	}
}



