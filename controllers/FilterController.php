<?php

require_once('models/Filter.php');

class FilterController
{

	public static function getDefaultFilterSection()
	{
		$default_filter = Filter::getDefault();

		$section = self::_getFilterSection(array($default_filter));
		set('section', $section);

		return partial('filter/show_filters.php');
	}

	public static function getCustomtFiltersSection()
	{
		$filters = Filter::fetchAllCustom();

		$section = self::_getFilterSection($filters);
		set('section', $section);

		return partial('filter/show_filters.php');

	}

	public static function updateFilters()
	{
		$updates = isset($_POST['update']) ? $_POST['update'] : array();
		$delete = isset($_POST['delete']) ? $_POST['delete'] : array();

		foreach ($updates as $filter_id => $filter_properties) {
			$filter = Filter::findById($filter_id);
			if ( ! $filter) {
				continue;
			}
			foreach ($filter_properties as $property => $value) {
				$filter->$property = $value;
			}
			$filter->save();
		}

		return partial('filter/update_filters.php');
	}

	protected static function _getFilterSection($filters)
	{
		$filters_table = new FilterTable();
		foreach ($filters as $filter) {
			$filters_table->addFilter($filter);
		}

		$form = new HtmlElement('form');
		$form->attr('method', 'post')
			->addChild($filters_table->toHtmlElement())
			->addChild(HtmlElement::factory('div')
				->attr('class', 'form-actions')
				->addChild(HtmlElement::factory('input')
					->attr('type', 'button')
					->attr('class', 'save')
					->attr('value', 'Save Changes')
					->attr('class', 'btn btn-primary')
				)
				->addChild(HtmlElement::factory('div')
					->attr('class', 'span4')
					->attr('id', 'submit_response')
				)
			);

		return $form;
	}

}
?>
