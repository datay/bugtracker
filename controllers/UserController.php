<?php

require_once('models/User.php');
require_once('models/UserPassword.php');
require_once('models/TracAccount.php');

class UserController
{

	public static function getUsersSection()
	{
		$users = User::fetchAll();

		$users_form = HtmlElement::factory('form')
			->attr('method', 'post')
			->addChild(self::_createUsersTable($users))
			->addChild(HtmlElement::factory('div')
				->attr('class', 'form-actions')
				->addChild(HtmlElement::factory('input')
					->attr('type', 'button')
					->attr('class', 'save')
					->attr('value', 'Save Changes')
					->attr('class', 'btn btn-primary')
				)
				->addChild(HtmlElement::factory('div')
					->attr('class', 'span4')
					->attr('id', 'submit_response')
				)
			);

		set('users_form', $users_form);
		return partial('user/show_users.php');
	}

	public static function getAddUserSection()
	{
		$add_user_form = self::_createAddUserForm();
		set('add_user_form', $add_user_form);

		return partial('user/get_add_user_section.php');
	}

	/**
	 * Expected parameter: 
	 * 	username, password, trac password, first_name, last_name
	 *
	 */
	public static function addUser()
	{
		$user_data = $_POST['user'];
		$user = new User();
		$user->username = $user_data['username'];
		$user->first_name = $user_data['first_name'];
		$user->last_name = $user_data['last_name'];
		$user->created_at = date('Y-m-d');
		$user->is_admin = isset($user_data['is_admin']) ? 't' : 'f';
		$user->save();

		$user_password = new UserPassword();
		$user_password->user_id = $user->user_id;
		$user_password->password = $user_data['password'];
		$user_password->save();

		$trac_account = new TracAccount();
		$trac_account->user_id = $user->user_id;
		$trac_account->pwd = $user_data['trac_password'];
		$trac_account->save();

		set('name', "{$user->first_name} {$user->last_name}");
		set('username', $user->username);	
		return partial('user/add_user.php');
	}

	/**
	 * Expected parameters:
	 * 	delete[$user_id]	-will delete if that user exists
	 * 	update[$user_id]
	 * 		-> ['password']
	 * 		-> ['tac_password']
	 *
	 */
	public static function updateUsers()
	{
		$updates = isset($_POST['update']) ? $_POST['update'] : array();
		$delete = isset($_POST['delete']) ? $_POST['delete'] : array();

		User::deleteByIds($delete);

		foreach ($updates as $user_id => $user_properties) {
			$user = User::findById($user_id);
			if ( ! $user) {
				continue;
			}

			foreach ($user_properties as $property => $value) {
				if ($property === 'trac_password') {
					$trac_account = $user->getTracAccount();
					$trac_account->pwd = $value;
					$trac_account->save();
				}
				else if ($property === 'password') {
					$user_password = $user->getUserPassword();
					$user_password->password = $value;
					$user_password->save();
				}
				else {
					$user->$property = $value;
				}
			}

			$user->save();
		}

		return partial('user/update_users.php');
	}

	protected static function _createUsersTable($users)
	{
		$headers = array('username', 'first_name', 'last_name');
		$accounts = array('password', 'trac_password');
		$thead_tr = HtmlElement::factory('tr');
		foreach ($headers as $header) {
			$thead_tr->addChild(HtmlElement::factory('th')
				->addChild(Formatter::titlize($header))
			);
		}
		foreach ($accounts as $header) {
			$thead_tr->addChild(HtmlElement::factory('th')
				->addChild(Formatter::titlize($header))
			);
		}
		$header = "Is Admin?";
		$thead_tr->addChild(HtmlElement::factory('th')
			->addChild(Formatter::titlize($header))
		);
		$thead_tr->addChild(HtmlElement::factory('th')
			->addChild('Delete')
		);

		$tbody = HtmlElement::factory('tbody');
		foreach ($users as $user) {
			$tr = HtmlElement::factory('tr');
			foreach ($headers as $header) {
				$data_name = "update[$user->user_id][$header]";

				$tr->addChild(HtmlElement::factory('td')
					->attr('class', 'control-group')
					->addChild(HtmlElement::factory('input')
						->prop('required')
						->attr('type', 'text')
						->attr('class', 'properties span2')
						->attr('value', $user->$header)
						->attr('data-name', $data_name)
						->attr('placeholder', Formatter::titlize($header))
					)
				);
			}

			foreach ($accounts as $header) {
				$data_name = "update[$user->user_id][$header]";

				$tr->addChild(HtmlElement::factory('td')
					->addChild(HtmlElement::factory('input')
						->attr('type', 'password')
						->attr('class', 'properties span2')
						->attr('placeholder', 'New Password')
						->attr('data-name', $data_name)
					)
				);
			}

			$header = "is_admin";
			$data_name = "update[$user->user_id][$header]";
			$options = array(
				TRUE => HtmlElement::factory('option')
					->attr('value', 't')
					->addChild('Yes'),
				FALSE => HtmlElement::factory('option')
					->attr('value', 'f')
					->addChild('No')
			);
			$options[$user->is_admin]->prop('selected');
			$select = HtmlElement::factory('select')
				->attr('data-name', $data_name)
				->attr('class', 'properties span2');
			foreach ($options as $option) {
				$select->addChild($option);
			}
			$tr->addChild(HtmlElement::factory('td')
				->addChild($select)
			);

			$data_name = "delete[]";
			$tr->addChild(HtmlElement::factory('td')
				->addChild(HtmlElement::factory('input')
					->attr('type', 'checkbox')
					->attr('class', 'properties delete')
					->attr('value', $user->user_id)
					->attr('data-name', $data_name)
				)
			);
			$tbody->addChild($tr);
		}

		return HtmlElement::factory('table')
			->attr('class', 'table table-collapsed')
			->addChild(HtmlElement::factory('thead')
				->addChild($thead_tr)
			)
			->addChild($tbody);
	}

	protected static function _createAdduserForm()
	{
		$information  = array(
			'information' => array(
				'username',
				'first_name',
				'last_name'
			)
		);
		$accounts = array(
			'accounts' => array(
				'password',
				'reenter_password',
				'trac_password',
				'reenter_trac_password'
			)
		);

		$add_user_form = HtmlElement::factory('form')
			->attr('method', 'post');

		foreach ($information as $fieldset_name => $fieldset_fields) {
			$fieldset = HtmlElement::factory('fieldset')
				->addChild(HtmlElement::factory('legend')
					->addChild(Formatter::titlize($fieldset_name))
				);
			foreach ($fieldset_fields as $field) {
				$name = "user[$field]";
				$fieldset->addChild(HtmlElement::factory('label')
					->addChild(Formatter::titlize($field))
					->addChild(HtmlElement::factory('input')
						->attr('name', $name)
						->attr('type', 'text')
					)
				);
			}

			$add_user_form->addChild($fieldset);
		}

		foreach ($accounts as $fieldset_name => $fieldset_fields) {
			$fieldset = HtmlElement::factory('fieldset')
				->addChild(HtmlElement::factory('legend')
					->addChild(Formatter::titlize($fieldset_name))
				);
			foreach ($fieldset_fields as $field) {
				$name = "user[$field]";
				$fieldset->addChild(HtmlElement::factory('label')
					->addChild(Formatter::titlize($field))
					->addChild(HtmlElement::factory('input')
						->attr('name', $name)
						->attr('type', 'password')
					)
				);
			}

			$add_user_form->addChild($fieldset);
		}
		//add is_admin checkbox
		$field = "is_admin";
		$name = "user[$field]";
		$fieldset->addChild(HtmlElement::factory('label')
			->addChild(Formatter::titlize($field))
			->addChild(HtmlElement::factory('input')
				->attr('name', $name)
				->attr('type', 'checkbox')
			)
		);

		return $add_user_form->addChild(HtmlElement::factory('div')
			->attr('class', 'form-actions')
			->addChild(HtmlElement::factory('input')
				->attr('type', 'button')
				->attr('class', 'save')
				->attr('value', 'Add')
				->attr('class', 'btn btn-success')
			)
			->addChild(HtmlElement::factory('div')
				->attr('class', 'span4')
				->attr('id', 'submit_response')
			)
		);
	}

}
?>
