<?php

class TicketController
{

	public static function createTicket()
	{
		$error_id = $_POST['create'];
		if ( ! $error_id) {
			return ':O';
		}
		$error = IrisError::findByErrorId($error_id);
		if ( ! $error) {
			return ':(';
			//do something
		}

		$user = option('current_user');
		$trac_account = $user->getTracAccount();
		$trac = Trac::getTrac();
		$trac->login($trac_account);
		$ticket_number = $trac->createTicket($error);
		$error->trac_ticket = $ticket_number;
		$error->owner = $trac_account->getUsername();
		$error->save();
		return (string)self::_createTicketLink($ticket_number);;
	}

	protected static function _createTicketLink($ticket_number)
	{
		$href = "https://trac.illuminateed.com/ticket/{$ticket_number}";
		$a = new HtmlElement('a');
		$a->attr('data-content', "Ticket #{$ticket_number}")
			->attr('href', $href)
			->attr('target', '_blank')
			->addChild("#{$ticket_number}");
		$div = new HtmlElement('div');
		$div->attr('style', 'text-align: center;')
			->addChild($a);

		return $div;
	}

}

?>
