<?php

class AccountController 
{

	public static function showAccount()
	{
		$user = option('current_user');
		$account_form = self::_createAccountForm($user);
		set('account_form', $account_form);

		return html('account/show_account.php');
	}

	public static function updateAccount()
	{
		$updates = isset($_POST['update']) ? $_POST['update'] : array();

		foreach ($updates as $user_id => $user_properties) {
			$user = User::findById($user_id);
			if ( ! $user) {
				continue;
			}

			foreach ($user_properties as $property => $value) {
				if ($property === 'trac_password') {
					$trac_account = $user->getTracAccount();
					$trac_account->pwd = $value;
					$trac_account->save();
				}
				else if ($property === 'password') {
					$user_password = $user->getUserPassword();
					$user_password->password = $value;
					$user_password->save();
				}
				else {
					$user->$property = $value;
				}
			}

			$user->save();
		}

		return partial('account/update_account.php');
	}

	protected static function _createAccountForm($user)
	{
		$information  = array(
			'update_information' => array(
				'username',
				'first_name',
				'last_name'
			)
		);
		$accounts = array(
			'update_accounts' => array(
				'password',
				'trac_password'
			)
		);

		$account_form = HtmlElement::factory('form')
			->attr('method', 'post');

		foreach ($information as $fieldset_name => $fieldset_fields) {
			$fieldset = HtmlElement::factory('fieldset')
				->addChild(HtmlElement::factory('legend')
					->addChild(Formatter::titlize($fieldset_name))
				);
			foreach ($fieldset_fields as $field) {
				$data_name = "update[$user->user_id][$field]";
				$fieldset->addChild(HtmlElement::factory('label')
					->addChild(Formatter::titlize($field))
					->addChild(HtmlElement::factory('input')
						->attr('data-name', $data_name)
						->attr('class', 'properties')
						->attr('type', 'text')
						->attr('value', $user->$field)
						->prop('required')
					)
				);
			}

			$account_form->addChild($fieldset);
		}

		foreach ($accounts as $fieldset_name => $fieldset_fields) {
			$fieldset = HtmlElement::factory('fieldset')
				->addChild(HtmlElement::factory('legend')
					->addChild(Formatter::titlize($fieldset_name))
				);
			foreach ($fieldset_fields as $field) {
				$data_name = "update[$user->user_id][$field]";
				$fieldset->addChild(HtmlElement::factory('label')
					->addChild(Formatter::titlize($field))
					->addChild(HtmlElement::factory('input')
						->attr('data-name', $data_name)
						->attr('class', 'properties')
						->attr('type', 'password')
					)
				);
			}

			$account_form->addChild($fieldset);
		}

		return $account_form->addChild(HtmlElement::factory('div')
			->attr('class', 'form-actions')
			->addChild(HtmlElement::factory('input')
				->attr('type', 'button')
				->attr('class', 'save')
				->attr('value', 'Save Changes')
				->attr('class', 'btn btn-primary')
			)
			->addChild(HtmlElement::factory('div')
				->attr('class', 'span4')
				->attr('id', 'submit_response')
			)
		);

	}
}
?>
