<?php

class AuthController
{
	public static function showLogin()
	{
		$username = isset($_POST['username']) ? $_POST['username'] : NULL;
		$password = isset($_POST['password']) ? $_POST['password'] : NULL;
		if ( ! $username && ! $password) {
			$message = '';
		}
		else {
			$message = self::_checkLoginCredentials($username, $password);
		}

		$login_form = self::_getLoginForm();
		set('message', $message);
		set('login_form', $login_form);
		return html('auth/show_login.php');
	}

	public static function login($user)
	{
		//add to sesion
		$bt_session = new Zend_Session_Namespace('bt');
		$bt_session->current_user = $user->user_id;
		option('current_user', $user);

		redirect_to('/');
	}

	public static function logout()
	{
		$bt_session = new Zend_Session_Namespace('bt');
		$bt_session->current_user = NULL;
		option('current_user', NULL);
		redirect_to('/login');
	}

	protected static function _checkLoginCredentials($username, $password)
	{
		if ( ! $username || ! $password) {
			return self::_getErrorMessage();
		}

		$user = User::findByUsername($username);
		if ( ! $user) {
			return self::_getErrorMessage();
		}

		$user_password = $user->getUserPassword();
		if ( ! $user_password->isValidPassword($password)) {
			return self::_getErrorMessage();
		}
		else {
			self::login($user);
		}
	}

	protected static function _getLoginForm()
	{
		return HtmlElement::factory('form')
			->attr('method', 'post')
			->attr('action', url_for('login'))
			->attr('class', 'well form-horizontal')
			->attr('style', 'padding-left: 35px; padding-bottom: 50px;')
			->addChild('USERNAME &nbsp;')
			->addChild(HtmlElement::factory('input')
				->attr('type', 'text')
				->attr('name', 'username')
				->attr('placeholder', 'Username')
			)
			->addChild('PASSWORD &nbsp;')
			->addChild(HtmlElement::factory('input')
				->attr('type', 'password')
				->attr('name', 'password')
				->attr('placeholder', 'Password') . "<br />"

			)
			->addChild(HtmlElement::factory('button')
				->attr('type', 'submit')
				->attr('class', 'btn btn-success')
				->attr('style', 'float: right; margin-top: 10px; margin-right: 30px;')
				->addChild('Login')
			);
	}

	protected static function _getErrorMessage()
	{
		return HtmlElement::factory('div')
			->attr('class', 'alert alert-error')
			->addChild('The username or password you entered is incorrect.');
	}
}

?>
