<?php

class AdminController
{
	public static function showSection()
	{
		//@todo: check if the user is admin
		$current_user = option('current_user');
		if ( ! $current_user->is_admin) {
			return html('admin/404.php');
		}
		//if not, show 404

		//an array of link to the section page
		$tabs = array(
			'default_filter',
			'custom_filters',
			'users',
			'add_user'
		);

		$sections = array();
		foreach ($tabs as $name) {
			$sections[$name] = self::_createNavTabLink($name);
		}

		$section_name = params('section');
		if ( ! $section_name) {
			$section_name = 'default_filter';
		}

		$method = '_show' . Formatter::toFunctionName($section_name);
		$section_tab = self::$method();

		set('sections', $sections);
		set('section_name', $section_name);
		set('section_tab', $section_tab);
		return html('admin/show_section.php');
	}

	protected static function _createNavTabLink($name)
	{
		return HtmlElement::factory('a')
			->attr('href', url_for('admin', $name))
			//->attr('data-toggle', 'tab')
			->addChild(Formatter::titlize($name));
	}

	protected static function _showDefaultFilter()
	{
		return FilterController::getDefaultFilterSection();
	}

	protected static function _showCustomFilters()
	{
		return FilterController::getCustomtFiltersSection();
	}

	protected static function _showUsers()
	{
		return UserController::getUsersSection();
	}

	protected static function _showAddUser()
	{
		return UserController::getAddUserSection();
	}

}
?>
