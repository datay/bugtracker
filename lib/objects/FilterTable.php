<?php

class FilterTable
{
	protected $_tbody;
	protected $_table;

	public function __construct()
	{
		$headers = array('title', 'delete');
		$thead_tr = HtmlElement::factory('tr');
		foreach ($headers as $header) {
			$thead_tr->addChild(HtmlElement::factory('th')
				->addChild(ucwords($header))
			);
		}

		$this->_tbody = $tbody = HtmlElement::factory('tbody');
		$this->_table = $table = HtmlElement::factory('table')
			->attr('class', 'table table-collapsed')
			->addChild(HtmlElement::factory('thead')
				->addChild($thead_tr)
			)
			->addChild($tbody);
	}

	public function addFilter($filter)
	{
		$this->_addRow($this->_createFilterDetails($filter));
		$this->_addRow($this->_createFilterOptions($filter));
	}

	public function toHtmlElement()
	{
		return $this->_table;
	}

	protected function _createFilterDetails($filter)
	{
		//add row for name and remove
		//assume there is title is the name

		$tr = HtmlElement::factory('tr')
			->addChild(HtmlElement::factory('td')
				->addChild($this->_createTitleInput($filter))
			)
			->addChild(HtmlElement::factory('td')
				->attr('rowspan', '2')
				->addChild($this->_createRemoveCheckbox($filter))
			);
		return $tr;
	}

	protected function _createFilterOptions($filter)
	{
		$filter_id = $filter->filter_id;

		$options = $filter->getOptionNames();
		$options = array_flip($options);

		$f_thead_tr = HtmlElement::factory('tr');
		$f_tr = HtmlElement::factory('tr');
		
		foreach ($options as $name => $val) {
			//add filter name as td
			$f_thead_tr->addChild(HtmlElement::factory('th')
				->addChild(ucwords($name))
			);

			//add the filter options as ul
			$data_name = "update[$filter_id][$name][]";
			$ul = HtmlElement::factory('ul')
				->attr('class', 'options_list')
				->attr('data-name', $data_name);

			$option_values = $filter->$name;
			foreach ($option_values as $option_value) {
				$ul->addChild(HtmlElement::factory('li')
					->addChild($option_value)
				);
			}
			$f_tr->addChild(HtmlElement::factory('td')
				->addChild($ul)
			);
		}

		return HtmlElement::factory('tr')
			->addChild(HtmlElement::factory('td')
				->addChild(HtmlElement::factory('table')
					->addChild(HtmlElement::factory('thead')
						->addChild($f_thead_tr)
					)
					->addChild(HtmlElement::factory('tbody')
						->addChild($f_tr)
					)
				)
			);
	}

	protected function _addRow($tr)
	{
		$this->_tbody->addChild($tr);
	}

	protected function _createTitleInput($filter)
	{
		$data_name = "update[$filter->filter_id][title]";
		$input = HtmlElement::factory('input')
			->attr('type', 'text')
			->attr('class', 'properties')
			->attr('value', $filter->title)
			->attr('data-name', $data_name);

		if ( ! $filter->is_custom) {
			$input->prop('readonly');
		}

		return $input;
	}

	protected function _createRemoveCheckbox($filter)
	{
		$name = "delete[$filter->filter_id]";
		$input = HtmlElement::factory('input')
			->attr('type', 'checkbox')
			->attr('id', $name)
			->attr('data-name', $name);

		if ( ! $filter->is_custom) {
			$input->prop('readonly');
		}

		return $input;
	}

}

?>
