<?php
/**
 * HTML element generator
 * Example usage:
 * require_once('HtmlElement.php');
 *
 * $input = new HtmlElement('input');
 * $input->attr('value', '711');
 * $input->attr('type', 'hidden');
 * $input->attr('id', 'acttion');
 * echo (string)$input . "\n";
 * 
 * $option = new HtmlElement('option');
 * $option->addChild('Dna Respositories');
 * $option->prop('selected');
 * $select = new HtmlElement('select');
 * $select->attr('name', 'component');
 * $select->attr('data-name', 'component[]');
 * $select->attr('id', 'component_id');
 * $select->addChild((string)$option);
 * echo (string)$select . "\n";
 */
class HtmlElement {
	protected $_attrs;
	protected $_props;
	protected $_tag_name;
	protected $_children;
	protected static $_self_closed_tags = array('input');

	public function __construct($tag_name)
	{
		$this->_attrs = $this->_props = $this->_children = array();
		$this->_tag_name = $tag_name;
	}

	/**
	 * Add a property to the element, eg: selected, disabled
	 *
	 * @param string $property_name
	 * return self
	 */
	public function prop($property_name)
	{
		$this->_props[$property_name] = $property_name;
		return $this;
	}

	/**
	 * Add an attribute to the element: eg: id, class, value
	 *
	 * @param string attribute
	 * @param string value
	 * @return self
	 */
	public function attr($attribute, $value)
	{
		$this->_attrs[$attribute][] = $value;
		return $this;
	}

	/**
	 * Add a child to the element. A child can be a text or another html element.
	 *  
	 * @param self | string
	 * @return self
	 */
	public function addChild($child)
	{
		$this->_children[] = $child;
		return $this;
	}

	public function __toString()
	{
		return $this->_build();
	}

	/**
	 * Build the html element as string. Internally called by __toString
	 *
	 * @return string
	 */
	protected function _build()
	{
		$props = array();
		/*
		foreach($this->_props as $prop) {
			$props[] = $prop . '="' . htmlentities($prop) . '"';
		}
		*/
		$props = $this->_props ? ' ' . implode(' ', $this->_props) : '';

		$attrs = array();
		foreach($this->_attrs as $attr_name => $attr_values) {
			$attr_values = implode(' ', $attr_values);
			$attrs[] = $attr_name . '="' . htmlentities($attr_values) . '"';
		}
		$attrs = $attrs ? ' ' . implode(' ', $attrs) : '';

		foreach ($this->_children as &$child) {
			if ($child instanceOf HtmlElement) {
				$child = (string)$child;
			}
		}
		$children = $this->_children ? implode('', $this->_children) : '';

		$start_tag = "<$this->_tag_name";
		if (in_array($this->_tag_name, self::$_self_closed_tags)) {
			$end_tag = ">";
			$start_tag_end = '';
		}
		else {
			$end_tag = "</$this->_tag_name>";
			$start_tag_end = '>';
		}

		return "{$start_tag}{$props}{$attrs}{$start_tag_end}{$children}{$end_tag}";
	}

	public static function factory($tag_name)
	{
		return new self($tag_name);
	}

}
?>
