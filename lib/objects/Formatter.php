<?php

class Formatter
{
	public static function titlize($name)
	{
		return ucwords(str_replace('_', ' ', $name));
	}

	public static function toFunctionName($fn_name)
	{
		//run_function to RunFunction
		return str_replace(' ', '', self::titlize($fn_name));
	}

}
?>
