<?php

require_once('./db/Db.php');

class IrisError {

	protected $_attributes = array(
		'error_id'		=> NULL,
		'message'               => NULL,
		'owner'			=> NULL,
		'type'			=> NULL,
		'status'		=> NULL,
		'component'		=> NULL,
		'count'			=> NULL,
		'version'		=> NULL,
		'administered_at'	=> NULL,
		'last_show'             => NULL,  
		'details'		=> NULL,
		'tags'			=> NULL,
		'trac_ticket'		=> NULL
	);

	protected $_with_defaults = array(
		'type',
		'status',
		'component',
		'count',
		'administered_at'
	);

	protected $_plain_text_details;

	protected $_db;
	protected $_changed;

	public function __construct($error_data = array())
	{
		$this->_db = Db::getInstance();
		foreach ($error_data as $column => $value) {
			$this->$column = $value;
		}
		$this->_changed = array();
	}

	protected function _update()
	{
		$attrs = $this->_changed;
		$columns = array();
		foreach ($attrs as $column => $value) {
			$columns[] = "{$column} = ?";
		}
		$columns = implode(', ', $columns);
		$attrs = array_values($attrs);
		$attrs[] = $this->_getAttribute('error_id');

		$sql = "
			UPDATE errors
			SET {$columns}
			WHERE error_id = ?";

		$this->_db->execute($sql, $attrs);
	}

	protected function _getAttribute($name)
	{
		return $this->_attributes[$name];
	}

	public function save()
	{
		$this->_update();
	}

	public function __set($attr, $val)
	{
		if ( ! array_key_exists($attr, $this->_attributes)) {
			throw new Exception("Unknown attribute $attr");
		}

		$this->_attributes[$attr] = $val;
		$this->_changed[$attr] = $val;
	}

	public function __get($attr)
	{
		if ( ! array_key_exists($attr, $this->_attributes)) {
			throw new Exception("Unknown attribute $attr");
		}

		return $this->_attributes[$attr];
	}

	/**
	 * Return stripped html tags the error details
	 *
	 * @return string
	 */
	public function getDetailsAsText()
	{
		if ( ! isset($this->_plain_text_details)) {
			$file_name = "/tmp/{$this->_getAttribute('error_id')}";
			file_put_contents($file_name, $this->_getAttribute('details'));
			$output_file = "{$file_name}_clean";
			$command = "links $file_name > $output_file";
			shell_exec($command);
			$this->_plain_text_details = file_get_contents($output_file);
		}

		return $this->_plain_text_details;
	}

	public function findBySubject($subject)
	{
		$subject = pg_escape_string($subject);
		$sql = "SELECT * FROM errors
			WHERE subject = '{$subject}'";

		$db = Db::getInstance();
		$error_data = $db->fetchObject($sql, NULL, 'IrisError');
		if ($error_data) {
			return new self($error_data);
		}
		return NULL;
	}

	public static function findByErrorId($error_id)
	{
		$sql = "
			SELECT *
			FROM errors
			WHERE error_id = '{$error_id}'
			ORDER BY last_show DESC
			";
		$error_data = Db::getInstance()->fetch($sql);
		if ( ! $error_data) {
			return NULL;
		}
		return new self($error_data);
	}

	public static function deleteByIds($ids)
	{
		$ids = implode(', ', $ids);
		$sql = "
			DELETE
			FROM errors
			WHERE error_id IN ({$ids})
			";
		return Db::getInstance()->exec($sql);
	}

	public static function fetchAll()
	{
		//details is intentionally excluded from the query
		$sql = "
			SELECT error_id, message, owner, count, version, administered_at, last_show, type, status, component, trac_ticket
			FROM errors
			ORDER BY count DESC
			";
		$db = Db::getInstance();
		$result = $db->fetchAll($sql);

		return $result ? $result : array();
	}

	public static function getBugList($options)
	{
		$db = Db::getInstance();
		if(count($options) == 1 || (! isset($options['option']) && ! isset($options['use_saved_filter']))) {
			return self::fetchRecent();
		}
		else {
			$saved_custom_options = array();
			if(isset($options['use_saved_filter']) && isset($options['save_filter'])) {
				foreach(array('owner', 'type', 'status', 'component') as $option) {
					$saved_filter_options = self::getOptionsFor($option, $options['save_filter']);	
					if( ! empty($saved_filter_options)) {
						$saved_custom_options[$option] = $saved_filter_options;
					}
				}	
			}

			if(isset($options['option']) && empty($saved_custom_options)) {
				$filter_options = $options['option'];
			}	
			else {
				$filter_options = $saved_custom_options;	
			}

			if( ! empty($filter_options)) {
				$filters =  array();	
				foreach($filter_options as $option => $values) {
					$filters[] = $option . ' IN (\'' . implode("', '", $values)	. '\') ';	
					$insert_new_options[] = '\'{' . implode(', ', $values) . '}\'';
				}
				$where = implode(' AND ', $filters);
				$insert_values = implode(', ', $insert_new_options);

				if(isset($options['save_as_filter']) && isset($options['title']) && ! isset($options['use_saved_filter'])) {
					$insert_into = implode(', ', array_keys($filter_options));
					$do_id = $db->fetchColumn("SELECT nextval('filters_seq')");
					$title = trim($options['title']);
					$sql = "
						INSERT INTO filters ($insert_into, filter_id, title) 
						VALUES($insert_values , $do_id, '$title')
						";
					$db->execute($sql);
				}
			}
			else {
				return self::fetchRecent();
			}
		}

		//details is intentionally excluded from the query
		$sql = "
			SELECT error_id, message, owner, count, tags, version, administered_at, last_show, type, status, component, trac_ticket
			FROM errors
			WHERE $where
			ORDER BY count DESC
			";
		$result = $db->fetchAll($sql);

		return $result ? $result : array();
	}

	public static function fetchRecent()
	{
		//details is intentionally excluded from the query
		$sql = "
			SELECT error_id, message, owner, count, tags, version, administered_at, last_show, type, status, component, trac_ticket
			FROM errors
			WHERE status <> 'closed'
			AND now() - last_show <= interval '7 days'
			";
		$db = Db::getInstance();
		$result = $db->fetchAll($sql);

		return $result ? $result : array();
	}

	public static function getOptionsFor($column, $id = 1)
	{
		static $options = array();

		if ( ! isset($options[$column])) {
			$sql = "
				SELECT {$column}
				FROM filters
				WHERE filter_id = $id
				";
			$db = Db::getInstance();
			$column_options = $db->fetchColumn($sql);
			//move the pgArray-phpArray conv to Db
			$column_options = str_replace('"', '', $column_options);
			$options[$column] = explode(',', trim($column_options, '{}'));
		}
		
		$column_options = $options[$column];
		if($column_options[0] == '') {
			$column_options = array();	
		}

		if($id != 1) {
			$options = array();	
		}

		return $column_options;
	}

	public static function getAllCustomOptions()
	{
		$sql = "
				SELECT filter_id, title
				FROM filters
				WHERE title <> 'Default'
			";
		$db = Db::getInstance();
		$custom_options = $db->fetchAll($sql);
		if($custom_options) {
			return $custom_options;	
		}
		else {
			return array();	
		}

		
	}
	
	// making this a separate function since the __get method becomes weird sometimes.
	public function getDetails()
	{
		return $this->_attributes['details'];
	}

	public static function getWinnerLoserData()
	{
		$sql = "
			SELECT component, sum(count) AS count, status
			FROM (
					SELECT component, count, 'winner' as status FROM (
						SELECT component, sum(count) as count, 'winner' as status 
						FROM errors 
						WHERE EXTRACT(WEEK FROM administered_at) = EXTRACT(WEEK FROM CURRENT_DATE) 
						AND EXTRACT(YEAR FROM administered_at) = EXTRACT(YEAR FROM CURRENT_DATE) 
						GROUP BY component) AS winners_count_less_than_three
					WHERE count < 3

					UNION

					SELECT component,'0' as count, 'winner' as status FROM (
						SELECT unnest(component) AS component
						FROM filters WHERE filter_id = 1
						) AS dc
					LEFT JOIN (
						SELECT component, sum(count) AS count 
						FROM errors
						WHERE 
						EXTRACT(WEEK FROM administered_at) <> EXTRACT(WEEK FROM CURRENT_DATE)
						AND EXTRACT(YEAR FROM administered_at) = EXTRACT(YEAR FROM CURRENT_DATE) 
						GROUP BY component
						) AS week_winner USING (component)
					WHERE component NOT IN (
							SELECT component FROM (
								SELECT component, sum(count) AS agg_count
								FROM errors 
								WHERE EXTRACT(WEEK FROM administered_at) = EXTRACT(WEEK FROM CURRENT_DATE)
								AND EXTRACT(YEAR FROM administered_at) = EXTRACT(YEAR FROM CURRENT_DATE) 
								GROUP BY component) AS losers
							WHERE agg_count > 3	
							)

					UNION

					SELECT component, count, 'loser' as status
					FROM (
							SELECT component, sum(count) AS count
							FROM errors 
							WHERE EXTRACT(WEEK FROM administered_at) = EXTRACT(WEEK FROM CURRENT_DATE)
							AND EXTRACT(YEAR FROM administered_at) = EXTRACT(YEAR FROM CURRENT_DATE) 
							GROUP BY component) AS losers
					WHERE count > 3

					) AS winner_loser 
					GROUP BY component, status
			";
		$db = Db::getInstance();
		$result = $db->fetchAll($sql);

		return $result ? $result : array();
	}

	public static function getWeeklyFrequency()
	{
		$sql = "
			SELECT EXTRACT(WEEK FROM administered_at), sum(count) AS entry_count FROM errors
			WHERE EXTRACT(YEAR FROM administered_at) = EXTRACT(YEAR FROM CURRENT_DATE)
			GROUP BY EXTRACT(WEEK FROM administered_at)
			";
		$db = Db::getInstance();
		$result = $db->fetchAll($sql);

		return $result ? $result : array();
	}

	public static function getBugsStatByComponent()
	{
		$sql = "
			SELECT * FROM (
				SELECT unnest(component) AS component, '0'::bigint AS total_count
				FROM filters WHERE filter_id = 1
			UNION
				SELECT component, sum(count) AS total_count
				FROM errors
				GROUP BY component
			) AS component_stat
			";
		$db = Db::getInstance();
		$result = $db->fetchAll($sql);

		return $result ? $result : array();
	}

}
?>
