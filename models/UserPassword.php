<?php

require_once('models/Entity.php');
require_once('models/User.php');

class UserPassword
extends Entity
{
	static $table_name = "user_passwords";
	static $primary_key = "user_password_id";

	protected $_attributes = array(
		'user_password_id'	=> NULL, //integer
		'user_id'		=> NULL, //integer
		'password'		=> NULL //character varying
	);

	public function getUser()
	{
		return User::findById($this->_getAttribute('user_id'));
	}

	public function isValidPassword($password)
	{
		$password = $this->_hashPassword($password, $this->getUser());

		return $password === $this->_getAttribute('password');
	}

	/**
	 * @param User $user
	 * @return self
	 */
	public static function findByUser(User $user)
	{
		$table_name = self::$table_name;
		$sql = "
			SELECT *
			FROM {$table_name}
			WHERE user_id = ?
		";
		$db = Db::getInstance();
		$data = $db->fetch($sql, array($user->user_id));

		return $data ? new self($data) : NULL;
	}

	protected function _setAttribute($attr, $val)
	{
		if ($attr === "password") {
			$val = $this->_hashPassword($val, $this->getUser());
		}

		parent::_setAttribute($attr, $val);
	}

	protected function _hashPassword($password, $user)
	{
		$algo = "sha256";
		$salt = array();
		foreach ($user->getAttributes() as $name => $value) {
			$salt[] = $value;
		}
		$salt[] = $password;

		return hash($algo, implode(':', $salt));
	}

}

?>
