<?php

/**
 * Base model for entities
 *
 */
class Entity
{

	/**
	 * Contains the key => value pair of changed attributes
	 *
	 * @var array
	 */
	protected $_changed = array();

	/**
	 * @var Db
	 */
	protected $_db;

	/**
	 *
	 * @oaram Db $db
	 */
	public function __construct($data = array(), $db = NULL)
	{
		foreach ($data as $attr => $val) {
			$this->_setAttribute($attr, $val);
		}	
		$this->_db = $db ? $db : Db::getInstance();
	}

	public function getAttributes()
	{
		return $this->_attributes;
	}

	public function getAttribute($attr)
	{
		return $this->_getAttribute($attr);
	}

	public function save()
	{
		return $this->_getAttribute(static::$primary_key) ? $this->_update() : $this->_insert();
	}

	public function __set($attr, $val)
	{
		$this->_setAttribute($attr, $val);
		$this->_setChanged($attr, $val);
	}

	public function __get($attr)
	{
		return $this->_getAttribute($attr);
	}

	public static function random()
	{
		$table_name = static::$table_name;
		$count_sql = "
			SELECT COUNT(*) 
			FROM {$table_name}
		";
		$count = Db::getInstance()->fetchColumn($count_sql);
		$sql = "
			SELECT *
			FROM {$table_name}
			OFFSET random() * {$count} - 1
			LIMIT 1
		";
		$data = Db::getInstance()->fetch($sql);
		return new static($data);
	}

	public static function findById($id)
	{
		$table_name = static::$table_name;
		$primary_key = static::$primary_key;
		$sql = "
			SELECT *
			FROM {$table_name}
			WHERE {$primary_key} = ?
		";
		$db = Db::getInstance();
		$result = $db->fetch($sql, array($id));
		if ( ! $result) {
			return NULL;
		}

		return new static($result);
	}

	public static function deleteByIds($ids)
	{
		$table_name = static::$table_name;
		$primary_key = static::$primary_key;

		$ids = implode(', ', $ids);
		$sql = "
			DELETE
			FROM {$table_name}
			WHERE {$primary_key} IN ({$ids})
			";
		return Db::getInstance()->exec($sql);
	}

	public static function fetchAll()
	{
		$table_name = static::$table_name;
		$sql = "
			SELECT *
			FROM {$table_name}
		";
		$db = Db::getInstance();
		$data = $db->fetchAll($sql);

		$all = array();
		foreach ($data as $datum) {
			$all[] = new static($datum);
		}
		return $all;
	}

	protected function _setAttribute($attr, $val)
	{
		if ( ! array_key_exists($attr, $this->_attributes)) {
			throw new Exception("Unknown attribute $attr");
		}

		$this->_attributes[$attr] = $val;
	}

	protected function _getAttribute($attr)
	{
		if ( ! array_key_exists($attr, $this->_attributes)) {
			throw new Exception("Unknown attribute $attr");
		}

		return $this->_attributes[$attr];
	}

	protected function _setChanged($attr, $val)
	{
		$this->_changed[$attr] = $val;
	}

	protected function _insert()
	{
		$primary_key = static::$primary_key;
		$attrs = $this->_attributes;
		unset($attrs[$primary_key]);

		$columns = array_keys($attrs);
		$columns = implode(', ', $columns);
		$values = array();
		foreach ($attrs as $column => $value) {
			$values[] = ":{$column}";
		}
		$values = implode(', ', $values);

		$table_name = static::$table_name;
		$primary_key = static::$primary_key;
		$sql = "
			INSERT INTO {$table_name} ($columns)
			VALUES ($values)
			RETURNING {$primary_key}
		";

		$db = Db::getInstance();
		$this->_setAttribute($primary_key, $db->fetchColumn($sql, $attrs));
	}

	protected function _update()
	{
		$table_name = static::$table_name;
		$primary_key = static::$primary_key;

		$attrs = $this->_changed;
		if ( ! $attrs) {
			return;
		}

		$columns = array();
		foreach ($attrs as $column => $value) {
			$columns[] = "{$column} = ?";
		}
		$columns = implode(', ', $columns);

		$attrs = array_values($attrs);
		$attrs[] = $this->_getAttribute($primary_key);

		$sql = "
			UPDATE {$table_name}
			SET {$columns}
			WHERE {$primary_key} = ?
		";

		$this->_db->execute($sql, $attrs);
	}

}
?>
