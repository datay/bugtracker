<?php

require_once('models/Entity.php');
require_once('models/User.php');

class TracAccount
extends Entity
{
	static $table_name = "trac_accounts";
	static $primary_key = "trac_account_id";

	protected $_attributes = array(
		'trac_account_id'	=> NULL, //integer
		'user_id'		=> NULL, //integer
		'pwd'			=> NULL //character varying
	);

	protected $_uid;

	public function __get($attr)
	{
		if ($attr === 'uid') {
			if ( ! isset($this->_uid)) {
				$this->_uid = $this->getUser()->username;
			}

			return $this->_uid;
		}

		parent::__get($attr);
	}

	public function getUser()
	{
		return User::findById($this->_getAttribute('user_id'));
	}

	public function getUsername()
	{
		return $this->getUser()->username;
	}

	public function getPassword()
	{
		return $this->_getAttribute('pwd');
	}

	/**
	 * @param User $user
	 * @return self
	 */
	public static function findByUser(User $user)
	{
		$table_name = self::$table_name;
		$sql = "
			SELECT *
			FROM {$table_name}
			WHERE user_id = ?
		";
		$db = Db::getInstance();
		$data = $db->fetch($sql, array($user->user_id));

		return $data ? new self($data) : NULL;
	}

	protected function _setAttribute($attr, $val)
	{
		if ($attr === "pwd") {
			$val = $this->_encrypt($val, $this->getUser());
		}

		parent::_setAttribute($attr, $val);
	}

	protected function _getAttribute($attr)
	{
		$val = parent::_getAttribute($attr);
		if ($attr == 'pwd') {
			$val = $this->_decrypt($val, $this->getUser());
		}

		return $val;
	}

	protected function _encrypt($password, $user)
	{
		//@todo
		return base64_encode($password);
	}

	protected function _decrypt($password, $user)
	{
		//@todo
		return base64_decode($password);
	}

}

?>
