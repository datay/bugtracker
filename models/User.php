<?php

require_once('models/Entity.php');
require_once('models/TracAccount.php');
require_once('models/UserPassword.php');

class User
extends Entity
{
	static $table_name = "users";
	static $primary_key = "user_id";

	protected $_attributes = array(
		'user_id'	=> NULL, //integer
		'first_name'	=> NULL, //character varying
		'last_name'	=> NULL, //character varying
		'username'	=> NULL, //character varying
		'created_at'	=> NULL, //date
		'is_admin'	=> FALSE //boolean
	);

	/**
	 *
	 * @return UserPassword
	 */
	public function getUserPassword()
	{
		return UserPassword::findByUser($this);
	}

	/**
	 *
	 * @return TracAccount
	 */
	public function getTracAccount()
	{
		return TracAccount::findByUser($this);
	}

	public static function findByUsername($username)
	{
		$table_name = static::$table_name;
		$sql = "
			SELECT *
			FROM {$table_name}
			WHERE username = ?
		";
		$db = Db::getInstance();
		$data = $db->fetch($sql, array($username));
		if ( ! $data) {
			return NULL;
		}

		return new static($data);
	}

	protected function _setAttribute($attr, $val)
	{
		if ($attr === "is_admin") {
			$val = $this->_setIsAdmin($val);
		}

		parent::_setAttribute($attr, $val);
	}

	protected function _getAttribute($attr)
	{
		$val = parent::_getAttribute($attr);
		if ($attr === "is_admin") {
			$val = $this->_getIsAdmin($val);
		}

		return $val;
	}

	protected function _getIsAdmin($is_admin)
	{
		if (is_string($is_admin)) {
			$is_admin = $is_admin === 't' ? TRUE : FALSE;
		}

		return $is_admin;
	}

	protected function _setIsAdmin($is_admin)
	{
		if (is_bool($is_admin)) {
			$is_admin = $is_admin ? 't' : 'f';
		}

		return $is_admin;
	}

}

?>
