<?php

require_once('./db/Db.php');

class ImportLog {

	public static function getLastImportDate() {
		$sql = "
			SELECT import_time
			FROM import_logs
			ORDER BY import_time DESC
			LIMIT 1
			";
		$db = Db::getInstance();
		return $db->fetchColumn($sql);
	} 
}
?>
