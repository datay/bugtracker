<?php

require_once('models/Entity.php');

class Filter
extends Entity
{
	static $table_name = "filters";
	static $primary_key = "filter_id";

	protected $_attributes = array(
		'filter_id'	=> NULL, //integer
		'type'		=> NULL, //array
		'owner'		=> NULL, //array
		'status'	=> NULL, //array
		'component'	=> NULL, //array
		'title'		=> NULL,  //character varying(250)
		'is_custom'	=> FALSE
	);

	/**
	 * These contains the column names that have array values
	 *
	 * @var array
	 */
	protected $_array_attributes = array(
		'type',
		'owner',
		'status',
		'component'
	);

	/**
	 * Get the names of options this set have
	 *
	 * @return indexed array
	 */
	public function getOptionNames()
	{
		return $this->_array_attributes;
	}

	/**
	 * Override so that we can convert array values to PHP arrays
	 *
	 */
	protected function _setAttribute($attr, $val)
	{
		if (in_array($attr, $this->_array_attributes) && is_array($val)) {
			$val = Db::toPgArray($val);
		}

		parent::_setAttribute($attr, $val);
	}

	protected function _getAttribute($attr)
	{
		$val = parent::_getAttribute($attr);
		if (in_array($attr, $this->_array_attributes)) {
			$val = Db::toArray($val);
		}

		return $val;
	}

	protected function _setChanged($attr, $val)
	{
		if (in_array($attr, $this->_array_attributes) && is_array($val)) {
			$val = Db::toPgArray($val);
		}

		parent::_setChanged($attr, $val);
	}

	public static function getDefault()
	{
		$default_option_title = "Default";
		$table_name = self::$table_name;
		$db = Db::getInstance();

		$sql = "
			SELECT *
			FROM {$table_name}
			WHERE title = ?
		";

		$data = $db->fetch($sql, array($default_option_title));
		return new self($data);
	}

	public static function fetchAllCustom()
	{
		$table_name = self::$table_name;
		$db = Db::getInstance();

		$sql = "
			SELECT *
			FROM {$table_name}
			WHERE is_custom
		";

		$data = $db->fetchAll($sql);
		$options = array();
		foreach ($data as $data) {
			$options[] = new self($data);
		}

		return $options;
	}
}
?>
