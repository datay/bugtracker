Updater = {
	listen: function(submit_selector, url) {
		var $submit = $(submit_selector);
		var $submit_form = $submit.closest('form');

		//add properties change listener
		$submit_form.on('change', '.properties', function() {
			var $this = $(this);
			if ($this.val()) {
				$this.attr('name', $this.data('name'))
				$this.parent().removeClass('error').addClass('success');
			}
			else {
				$this.removeAttr('name');
				$this.parent().removeClass('success').addClass('error');
			}

		});
	
		//add submit listener, do an AJAX POST instead of form submit
		$submit.click(function() {
			var $this = $(this);
			$this.prop('disabled', true).text('Saving...');

			var data = $submit_form.serialize();
			$.post(url, data, function(post_data) {
				$('.delete[name]').closest('tr').remove();

				//remove the name attributes
				$('.properties[name]').each(function() {
					var $this = $(this);
					$this.removeAttr('name');
				});

				//@todo: show returned data beside the save button
				var $message = $('<div/>', {
					'class': 'alert alert-success',
					'id': 'submit_response',
					'html': post_data
				});
				$('#submit_response').replaceWith($message);
				$message.delay(3000).fadeOut();

				$this.fadeOut(100, function() {
					$this.text('Save Changes').prop('disabled', false).show();
				});
			});
		});
	}
}
