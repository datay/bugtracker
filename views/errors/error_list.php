<style type="text/css">
#filter_container {
	margin-bottom: 20px;
	border: 1px solid #DDDDDD;
  	border-radius: 5px 5px 5px 5px;
}
table.filter_table {
	width: 100%;
}
tr.with_border td {
}
#option_header td {
	font-weight: bolder;
	padding: 10px 25px;
	color:  #272924;
}
.option_list {
	list-style: none;
	overflow: auto;
	height: 90px;
	padding-right: 5px;
}
.filter_option {
	display: block;
	float: left;
	margin: 2px 5px 2px 0;
	font-weight: bold;
	border: 1px solid #DDDDDD;
	background-color: #F9F9F9;
	color: #5B5E56;
	padding: 2px 5px 3px;
	cursor: pointer;
  	border-radius: 5px;
}
.filter_option:hover, .selected {
	background-color: #B1E0E3;
	border: 1px solid #73D2D9;
	font-weight: bold;
}	
#filter_controls {
	border-top: 1px solid #DDDDDD;
}
#filter_controls td {
	padding: 10px 10px 0;
}
.error_properties {
	width: 80px;	
}
.popover-inner {
	width: 600px;	
}
.popover {
	width: 500px;	
}
</style>
<script>
	$(document).ready(function () {
		var $form = $('form');
		$form.on('change', '.error_properties', function () {
			var $target = $(this);
			if ($target.val() != $target.data('default')) {
				$target.attr('name', $target.data('name'));
			}
			else {
				$target.removeAttr('name');
			}
		});
		$form.on('change', '.delete_checkboxes', function () {
			var $target = $(this);
			if ($target.prop('checked')) {
				$target.attr('name', $target.data('name'));
			}
			else {
				$target.removeAttr('name');
			}
		});
		$form.on('click', '.create_ticket', function() {
			var $this = $(this);
			var $td = $this.closest('td');
			$this = $this.replaceWith('<div class="progress progress-striped active"><div class="bar" style="width: 100%;"></div></div>');
			var $create_ticket = $('#create_ticket');
			$create_ticket.attr('name', 'create').val($this.val());
			var data = $create_ticket.serialize();
			$.post('<?php echo url_for('create_ticket') ?>', data, function(data) {
				$create_ticket.removeAttr(name).removeAttr('value');
				$td.html(data);
			});
		});
		$('a.messages').popover({
			placement: 'bottom'
		});

		$('.checkbox_option')
		.filter(':has(:checkbox:checked)')
		.addClass('selected')
		.end()
		.click(function (e) {
			$(this).toggleClass('selected');
			if(e.target.type !== 'checkbox') {
				$(':checkbox', this).attr('checked', function() {
					return !this.checked;	
				});

			}
		});

		$('.radio_saved_option')
		.filter(':has(:radio:checked)')
		.addClass('selected')
		.end()
		.click(function (e) {
			$('.radio_saved_option').removeClass('selected');
			$(this).toggleClass('selected');
			if(e.target.type !== 'radio') {
				$(':radio', this).attr('checked', function() {
					return !this.checked;	
				});
			}
		});
		
		showSaveFilter($('#use_saved_filter'));
		
		$('#use_saved_filter').click(function() {
			showSaveFilter($(this));
		});
		$('#save_as_filter').click(function() {
			if($(this).prop('checked')) {
				$(this).next('input').show('slow');
				$(this).next().next('span').hide('slow');
			}
			else {
				$(this).next('input').hide('slow');
				$(this).next().next('span').show('slow');
			}
		});
	});
	function showSaveFilter($this) {
		if($this.prop('checked')) {
			$('#default_filter, #option_header, #save_as_filter_cont').hide();	
			$('#saved_filter').show()
		}	
		else {
			$('#default_filter, #option_header, #save_as_filter_cont').show();	
			$('#saved_filter').hide()
			
		}
	}
</script>
<div id="error_content" style="display: none;">
	<div id="import_info" style="text-align: right; padding-bottom: 10px;">
		<b>Last Import:</b> <?php echo $last_import ?>
	</div>
	<div id="filter_container">
	<form method="post" action="<?php echo url_for('update_filter'); ?>">
	<table class="filter_table">
		<tr id="option_header" class="with_border">
			<td>Owner</td><td>Type</td><td>Status</td><td style="width: 48%;">Component</td>
		</tr>
		<tr id="default_filter" class="with_border">
			<?php 
				$selected_filters = ( ! $use_saved_filter && ! is_null($selected_filters)) ? $selected_filters : array();

				foreach(array('owner', 'type', 'status', 'component') as $option_name) {
					$options = IrisError::getOptionsFor($option_name); 
					echo '<td><ul class="option_list">';
					foreach($options as $option) {
						$selected_class = ( ! empty($selected_filters) && array_key_exists($option_name, $selected_filters) && in_array($option, $selected_filters[$option_name])) ? 'selected' : NULL;
						$checked = ( ! empty($selected_filters) && array_key_exists($option_name, $selected_filters) && in_array($option, $selected_filters[$option_name])) ? 'checked="checked"' : NULL;

						echo '<li class="checkbox_option filter_option ' . $selected_class . '"><input style="display: none;" type="checkbox" ' . $checked  .  ' class="filter_checkbox" name="option[' . $option_name  .'][]" value="' . $option . '">' . $option . '</li>';
					}
					echo '</ul></td>';

				}
			?>
		</tr>
		<?php $checked = $use_saved_filter ? 'checked="checked"' : NULL; ?>
		<tr id="saved_filter" class="with_border" style="display: none; ?>">
			<td colspan="4"><ul class="option_list" style="padding-top: 10px;">
			<?php 

				$saved_filters = IrisError::getAllCustomOptions();
				if( ! empty($saved_filters)) {
					foreach($saved_filters as $id => $filter) { 
						$selected_class = ($use_saved_filter && $selected_saved_filter == $filter['filter_id']) ? 'selected' : NULL; 
						echo '<li class="radio_saved_option filter_option ' . $selected_class  . '"><input name="save_filter" type="radio" style="display: none;" value="' . $filter['filter_id'] . '">' . $filter['title'] . '</li>'; 	
						
					}
				}
			?>
			</ul></td>
		</tr>
		<tr id="filter_controls">
			<td><input type="checkbox" id="use_saved_filter" name="use_saved_filter" value="1" <?php echo $checked; ?>> Use Saved Filter</td>
			<td colspan="3" style="text-align: right;">
				<span id="save_as_filter_cont">
					<input type="checkbox" id="save_as_filter" name="save_as_filter" value="1">
					<input type="text" name="title" placeholder="Filter Name" style="display: none; margin-bottom: 0;">
					<span style="padding-right: 5px;">Save As a New Filter</span> 
				</span>
				<span><button id="update_filter" class="btn">Submit</button></span>
			</td>
		</tr>
	</table>
	</form>
	</div>
	<form method="post">
	<input type="hidden" name="_method" value="PUT" id="_method">
	<input type="hidden" id="create_ticket">
	<table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered" id="errors">
		<thead>
			<?php 
			echo "<tr>";
			foreach ($error_headers as $error_header) {
				$width = NULL;
				if($error_header == 'Message') {
					$width = 'style="width: 350px;"';	
				}
				echo "<th {$width}>{$error_header}</th>";
			}
			echo "</tr>";
			?>
		</thead>
		<tbody>
			<?php 
			foreach ($errors as $error_id => $error) {
				echo "<tr>";
				foreach ($error as $error_key => $error_attribute) {
					$ta = NULL;
					if($error_key == 'error_id') {
						$ta = 'style="text-align: center;"';	
					}
					echo "<td {$ta}>{$error_attribute}</td>";
				}
				echo "</tr>";
			}
			?>
		</tbody>
	</table>
	<div style="float: right; width: %;" id="table-actions">
	<select name="action" style="width: 100px; margin-top: 10px;"><option>Delete</option><option>Merge</option></select>
	<button type="button" class="btn btn-primary" id="save">Save changes</button>
	</div>
	</form>
</div>
