<style type="text/css">
#toTop {
  width:100px;
  border:1px solid #ccc;
  background:#f7f7f7;
  text-align:center;
  padding:5px;
  position:fixed; /* this is the magic */
  bottom:10px; /* together with this to put the div at the bottom*/
  right:10px;
  cursor:pointer;
  display:none;
  color:#333;
  font-family:verdana;
  font-size:11px;
}
</style>
<script type="text/javascript">
$(function() {
		$(window).scroll(function() {
			if($(this).scrollTop() != 0) {
			$('#toTop').fadeIn();	
			} else {
			$('#toTop').fadeOut();
			}
			});

		$('#toTop').click(function() {
			$('body,html').animate({scrollTop:0},800);
			});	
		});
</script>
<div style="overflow: auto; background-color: #F5E1E9; padding: 10px 5px;">
<div id="toTop">^ Back to Top</div>
<?php echo $details; ?>
</div>
