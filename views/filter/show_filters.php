<script>
	$(document).ready(function() {
		$('.options_list').tagit({
			'sortable': true,
			'caseSensitive': true,
			'triggerKeys': ['enter', 'comma', 'tab'],
			'select': true,
			'highlightOnExistColor': '#F00'
		});
		Updater.listen('.save', '<?php echo url_for('update_filters') ?>');
	});
</script>
<?php echo $section ?>
