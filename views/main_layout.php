<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html lang="en">
<head>
<meta charset="utf-8"/>
<title>Bugs do Bite!</title>

<!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
<!--[if lt IE 9]>
<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->

<!-- Le fav and touch icons -->
<style type="text/css">
#content {
  background: none repeat scroll 0 0 #FFFFFF;
  border-radius: 0 0 5px 5px;
  box-shadow: 3px 3px 5px #202121;
  color: #000000;
  font-family: "Lucida Grande","Helvetica Neue",helvetica,arial,sans-serif;
  font-size: 11px;
  line-height: 16px;
  padding: 20px 20px 60px;
  vertical-align: top;
}
#content select {
  color: #000000;
  font-family: "Lucida Grande","Helvetica Neue",helvetica,arial,sans-serif;
  font-size: 11px;
  line-height: 16px;
}
</style>
<script src="../bugtracker/assets/datatable/js/jquery.js" type="text/javascript"></script>

<link rel="stylesheet" type="text/css" href="../bugtracker/assets/bootstrap/css/bootstrap.min.css"/>
<script src="../bugtracker/assets/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="../bugtracker/assets/bootstrap/js/bootstrap-tooltip.js" type="text/javascript"></script>
<script src="../bugtracker/assets/bootstrap/js/bootstrap-popover.js" type="text/javascript"></script>

<link rel="stylesheet" type="text/css" href="../bugtracker/assets/datatable/css/bootstrap-responsive.css"/>
<link rel="stylesheet" type="text/css" href="../bugtracker/assets/datatable/css/DT_bootstrap.css"/>
<script src="../bugtracker/assets/datatable/js/jquery.dataTables.min.js" type="text/javascript"></script>
<script src="../bugtracker/assets/datatable/js/DT_bootstrap.js" type="text/javascript"></script>

<link rel="stylesheet" type="text/css" href="../bugtracker/assets/tagit/css/tagit-simple-grey.css"/>
<script src="../bugtracker/assets/bootstrap/js/bootstrap-tab.js" type="text/javascript"></script>
<script src="../bugtracker/assets/tagit/jqueryui/js/jquery-ui-1.8.22.custom.min.js" type="text/javascript"></script>
<script src="../bugtracker/assets/tagit/js/tagit.js" type="text/javascript"></script>

<script src="../bugtracker/assets/js/update.js" type="text/javascript"></script>

</head>
<body style="background-color: #96ADB0; background-image: url('../bugtracker/assets/images/low_contrast_linen.png'); padding: 40px; 30px;">
<!--<body style="padding: 30px;">-->
<div class="navbar navbar-fixed-top">
	<div class="navbar-inner">
		<div class="container">
			<a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</a>
			<a class="brand" href="<?php echo url_for(''); ?>"><span style="color: #F57451; font-weight: bold;">Bug</span><span style="color: #F2F8FA; font-weight: bold;">Tracker</span>&nbsp;<sup class="badge badge-info">v0.5</sup></a>
			<div class="nav-collapse">
				<ul class="nav">
				<?php if ($user = option('current_user')): ?>
					<li><a href="<?php echo url_for(''); ?>">Bugs</a></li>
					<li><a href="<?php echo url_for('statistics');?>">Statistics</a></li>
					<?php if ($user->is_admin):?>
						<li><a href="<?php echo url_for('admin'); ?>">Admin</a></li>
					<?php endif ?>
					<li><a href="<?php echo url_for('account'); ?>">Account</a></li>
					<li><a href="<?php echo url_for('logout'); ?>">Log out</a></li>
				<?php endif ?>
				</ul>
				<ul class="nav pull-right">
					<li>
						<div style="width: 368px; text-align: right; padding: 5px; color: #F2F1D0; ">
							<span style="font: Palatino,serif; font-style: italic; font-size: 10px;">Man works for an object. Remove that object and you reduce him into inaction. </span> 
							<span style="font-size: 10px;"> - Indolence of the Filipino - La Solidaridad -1890 </span>
						<!--<div style="margin-top: -35px; margin-right: -35px;"><img src="assets/images/logo_5.0_header.png"/></div>-->
						</div>	
					</li>
				</ul>
			</div><!--/.nav-collapse -->
		</div>
	</div>
</div>

<div class="container">
<div id="content">
	<?php echo $content;?>
</div>
</div>

</body>
</html>

