<style tyle="text/css">
.candidate {
	margin-bottom: 10px;	
}
.charts {
  border-radius: 0 0 5px 5px;
  box-shadow: 3px 3px 5px #202121;
}
</style>
<script type="text/javascript" src="https://www.google.com/jsapi"></script>
<script type="text/javascript">
google.load("visualization", "1", {packages:["corechart"]});
google.setOnLoadCallback(drawChart);
function drawChart() {
	var data = google.visualization.arrayToDataTable(<?php echo json_encode($weekly_trend); ?>);

	var options = {
		title: 'Weekly Bug Trend',
		vAxis: {title: 'Week'},
		hAxis: {title: 'Number of Bugs'},
		legend: {position: 'none'},
		backgroundColor : {fill: '#F3FAF0'}

	};

	var chart = new google.visualization.LineChart(document.getElementById('chart_div'));
	chart.draw(data, options);

	var data = google.visualization.arrayToDataTable(<?php echo json_encode($bug_stat); ?>);

	var options = {
		title: 'Bug Frequency By Component',
		backgroundColor : {fill: '#F3FAF0'}
	};

	var chart = new google.visualization.PieChart(document.getElementById('chart_div_bar'));
	chart.draw(data, options);
}
</script>


<?php 
	$winners = NULL;
	$losers = NULL;
	foreach($winner_loser as $details) {
		$html = "<tr><td>{$details['component']}</td><td><span class=\"badge badge-info\">{$details['count']}</span></td></tr>";
		if($details['status'] == 'winner') {
			$winners .= $html;
		}	
		else {
			$losers .= $html;
		}
	}

?>
<div>
<div class="row">
	<div class="span6" style="margin-left:50px;">
		<div class="candidate">
			<span class="badge badge-success">Winners for this Week</span>
		</div>
		
		<div>
			<div class="alert alert-heading" style="margin-bottom: 20px; min-height: 180px;">
				<table class="table">
				<thead>
					<th>Component</th>
					<th>Bug Count</th>
				</thead>
				<tbody>
					<?php echo $winners; ?>
				</tbody>
				</table>
			</div>
		</div>

	</div>
	<div class="span6">
		<div class="candidate">
			<span class="badge badge-warning">Losers for this Week</span>
		</div>

		<div class="alert alert-heading" style="margin-bottom: 20px; min-height: 180px;">
			<table class="table">
			<thead>
				<th>Component</th>
				<th>Bug Count</th>
			</thead>
			<tbody>
				<?php echo $losers; ?>
			</tbody>
			</table>
		</div>

	</div>
</div>
<div class="row" style="margin-left: 120px;">
	<div class="span10">
		<div class="thumbnail">
			<div id="chart_div" style="width: auto; height: 330px;"></div>
		</div>
	</div>
</div>
<div class="row" style="margin-left: 120px; margin-top: 10px;">
	<div class="span10">
		<div class="thumbnail">
			<div id="chart_div_bar" style="width: auto; height: 330px;"></div>
		</div>
	</div>
</div>
</div>
