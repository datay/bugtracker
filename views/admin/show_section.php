<script>
</script>
<div class="">
	<ul class="nav nav-tabs">
		<?php foreach ($sections as $name => $link): ?>
		<li class="<?php echo $name === $section_name ? 'active' : ''?>">
			<?php echo $link ?>
		</li>
		<?php endforeach ?>
	</ul>
	<h3><?php echo Formatter::titlize($section_name) ?></h3>
	<div class="tab-content" style="padding-bottom: 9px; border-bottom: 1px solid #ddd;">
		<div class="" id="<?php echo $section_name; ?>">
			<?php echo $section_tab; ?>
		</div>
	</div>
</div>

