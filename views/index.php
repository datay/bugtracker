<script>
	$(document).ready(function() {
		$.get('<?php echo url_for('error_list') ?>', <?php echo json_encode($post); ?>, function (data) {
			var $content = $('#content');
			$content.append(data);
			var $table = $('#errors').dataTable({
				"sDom": "<'row'<'left-controls'f><'right-controls'lpi>t",
				"sPaginationType": "bootstrap",
				"oLanguage": {
					"sLengthMenu": "_MENU_",
					"sInfo" : "_START_-_END_ of _TOTAL_",		
					"sSearch" : ""
				},
				"iDisplayLength": 25,
				'aaSorting': [[2, "desc"], [0, "desc"]],
				'aoColumnDefs' : [
					{"bSortable" : false, "aTargets" :[7, 8]},
					{"bVisible" : false, "aTargets" : [2]}
				],
				'fnRowCallback': function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
					//index 1 is count
					//do the styling here since we want to preserve the numeric sorting
					var count = aData[0];
					var span_class = 'badge';
					if (count == 3 || count == 2) {
						span_class += ' badge-warning';
					}
					else if (count > 3) {
						span_class += ' badge-error';
					}
					$('td:eq(0)', nRow).html($('<span/>', {
						text: count,
						'class': span_class
					}));
				}
			});
			$('.progress').fadeOut(500, function() {
				$('#error_content').fadeIn(500);
			});

			$('#save').click(function () {
				var $this = $(this);
				$this.prop('disabled', true).text('Saving...');
				var data = $('form').serialize();
				//check if there is something to save
				$.post('<?php echo url_for('error_update') ?>', data, function (data) {
					if($("select[name='action']").val() == 'Delete') {
						//delete the selected rows
						$('.delete_checkboxes[name]').each(function() {
							var $row = $(this).closest('tr').get(0);
							$table.fnDeleteRow($table.fnGetPosition($row));
						});
					}
					else {
						$('.delete_checkboxes[name]').each(function() {
							var $row = $(this).closest('tr').get(0);
							if($(this).val() == $(data).find('#retained').val()) {
								$(this).prop('checked', false);
								$(this).closest('tr')
									.find('td.sorting_2 span.badge').text($(data).find('#retained').attr('count')).end();
								$(this).closest('td').append($(data).html());
								$('.note').fadeOut(2000);
							}
							else {
								$table.fnDeleteRow($table.fnGetPosition($row));
							}
						});
					}

					$('.error_properties[name]').each(function() {
						var $this = $(this);
						//remove name and set data-default to the current value
						$this.removeAttr('name');
						$this.data('default', $this.val());
					});
					//set the default of error_properties
					$this.fadeOut(500, function() {
						//delete selected rows
						$this.text('Save Changes').prop('disabled', false).fadeIn(500);
					});
				});
			});

		});
	});
</script><?php
echo $loading;
?>
</div>
