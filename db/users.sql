﻿DROP TABLE trac_accounts;
DROP TABLE user_passwords;
DROP TABLE users;
DROP SEQUENCE trac_accounts_seq;
DROP SEQUENCE user_passwords_seq;
DROP SEQUENCE users_seq;

CREATE SEQUENCE users_seq;
CREATE TABLE users
(
  user_id integer NOT NULL DEFAULT nextval('users_seq'::regclass),
  first_name character varying(200) NOT NULL,
  last_name character varying(200) NOT NULL,
  username character varying(200) NOT NULL,
  created_at timestamp without time zone NOT NULL DEFAULT ('now'::text)::date,
  is_admin boolean NOT NULL DEFAULT false,
  CONSTRAINT user_id_pk PRIMARY KEY (user_id ),
  CONSTRAINT username_uk UNIQUE (username )
);

CREATE SEQUENCE trac_accounts_seq;
CREATE TABLE trac_accounts
(
  trac_account_id integer NOT NULL DEFAULT nextval('trac_accounts_seq'::regclass),
  user_id integer NOT NULL,
  pwd character varying(255) NOT NULL,
  CONSTRAINT trac_account_pk PRIMARY KEY (trac_account_id ),
  CONSTRAINT trac_account_fk FOREIGN KEY (user_id)
      REFERENCES users (user_id) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE CASCADE,
  CONSTRAINT trac_account_uk UNIQUE (user_id )
);

CREATE SEQUENCE user_passwords_seq;
CREATE TABLE user_passwords
(
  user_password_id integer NOT NULL DEFAULT nextval('user_passwords_seq'::regclass),
  user_id integer NOT NULL,
  password character varying(255) NOT NULL,
  CONSTRAINT user_id_fk FOREIGN KEY (user_id)
      REFERENCES users (user_id) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE CASCADE
);
