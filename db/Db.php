<?php

class Db {

	protected static $_instance;

	protected $_connection;

	protected function __construct()
	{
		$env = "pgsql:dbname=dna_errors;host=192.168.1.88"; 
		try {
			$db = new PDO($env, 'postgres', '');
		}
		catch(PDOException $e) {
			//do something
			halt("Connexion failed: ".$e); # raises an error / renders the error page and exit.
		}
		$db->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING );

		$this->_connection = $db;
	}

	public function fetchAll($sql, $params = array())
	{
		$query = $this->_getQuery($sql, $params);
		return $query->fetchAll(PDO::FETCH_ASSOC);
	}

	public function fetch($sql, $params = array())
	{
		$query = $this->_getQuery($sql, $params);
		return $query->fetch(PDO::FETCH_ASSOC);
	}

	public function fetchColumn($sql, $params = array())
	{
		$query = $this->_getQuery($sql, $params);
		return $query->fetchColumn();
	}


	public function execute($sql, $params = array())
	{
		$query = $this->_connection->prepare($sql);
		return $query->execute($params);
	}

	public function insert($sql, $params)
	{
		$query = $this->_getQuery($sql, $params);
		return $this->_connection->lastInsertId();
	}

	public function exec($sql)
	{
		return $this->_connection->exec($sql);
	}

	protected function _getQuery($sql, $params)
	{
		$query = $this->_connection->prepare($sql);
		$query->execute($params);

		return $query;
	}

	public static function getInstance()
	{
		if ( ! self::$_instance) {
			self::$_instance = new Db();
		}

		return self::$_instance;
	}

	/**
	 * Convert a Postgres array, represented as string in PHP, to PHP array
	 *
	 * @param string $str_array
	 * @return array
	 */
	public static function toArray($str_array)
	{
		$str_array = str_replace('"', '', $str_array);
		return explode(',', trim($str_array, '{}'));
	}

	/**
	 * Inverse of toArray
	 *
	 */
	public static function toPgArray($array)
	{
		if ( ! is_array($array)) {
			throw new exception("Not an array.");
		}

		return '{' . implode(', ', $array) . '}';
	}

}

?>
