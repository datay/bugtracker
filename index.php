<?php

require_once('lib/limonade.php');
require_once('Zend/Session.php');
require_once_dir('lib/objects');
require_once_dir('models');

Zend_Session::start();

function configure() 
{
	option('env', ENV_DEVELOPMENT);

	$bt_session = new Zend_Session_Namespace('bt');
	if (isset($bt_session->current_user)) {
		$user_id = $bt_session->current_user;
		$user = User::findById($user_id);
		if ($user) {
			option('current_user', $user);
		}
	}
}

function before()
{
	$login_uri = "/login";
	if ( ! option('current_user') && request_uri() != $login_uri) {
		redirect($login_uri);
	}
	else {
		layout('main_layout.php');
	}
}

function _formatStringDate($date, $show_time = FALSE, $format = 'm/d/Y')
{
	$format = $format . ($show_time ? ' g:i a' : '');
	return date($format, strtotime($date));
}

function _formatErrorData(array $iris_errors)
{
	//@todo: move this to controller
	//get the errors with contents
	$max_characters = 50;
	$formatted_errors = array();
	$select_columns = array('type', 'status', 'owner', 'component');
	$skipped_columns = array('administered_at', 'version');
	foreach ($iris_errors as $iris_error) {
		$errors = array_fill_keys(array(
			'count',
			'message',
			'last_show',
			'owner',
			'type',
			'status',
			'component',
			'trac_ticket',
			'error_id',
		), NULL);
		foreach ($iris_error as $error_key => $error_attribute) {
			if (in_array($error_key, $skipped_columns)) {
				continue;
			}
			
			//format the value to show
			if ($error_key === 'administered_at' || $error_key === 'last_show') {
				$error_attribute = _formatStringDate($error_attribute);
			}
			else if (in_array($error_key, $select_columns)) {
				//create a select error_attribute
				$name = 'update[' . $iris_error['error_id'] . "][{$error_key}]";

				$select_el = new HtmlElement('select');
				$select_el->attr('data-name', $name)
					->attr('data-default', $error_attribute)
					->attr('class', $error_key)
					->attr('class', 'span2')
					->attr('class', 'error_properties')
					->attr('style', 'width: 120px;');

				$options = IrisError::getOptionsFor($error_key);
				foreach ($options as $option) {
					$option_el = new HtmlElement('option');
					if ($option === $error_attribute) {
						$option_el->prop('selected');
					}
					$option_el->addChild($option);
					$select_el->addChild($option_el);
				}
				$error_attribute = (string)$select_el;
			}
			else if ($error_key === 'error_id') {
				//add checkbox with no name yet
				$name = "delete[]";
				$input_el = new HtmlElement('input');
				$input_el->attr('type', 'checkbox')
					->attr('class', 'delete_checkboxes')
					->attr('data-name', $name)
					->attr('value', $error_attribute);
				$error_attribute = (string)$input_el;
			}
			else if ($error_key === 'message') {
				$list = $iris_error['tags'];
				$hints = explode(',', $list);
				$hints_form = NULL;
				foreach($hints as $hint) {
					$hint = trim($hint);
					if($hint == '') {
						continue;	
					}
					$hint_span = new HtmlElement('span');
					$hint_span
						->attr('class', 'alert alert-info')
						->attr('style', 'padding: 3px; font-weight: normal; font-size: 7pt; margin-right: 2px;')
						->addChild($hint);
					$hints_form .= (string) $hint_span;
				}
				if($hints_form != '') {
					$hints_form = '<span style="margin-bottom: 15px; font-weight: bolder;">Hints</span><br/>' . $hints_form . '<br /><br />';
				}
				
				//format the message to show only 50 characters but with full title attribute
				$content = strlen($error_attribute) > $max_characters
					? substr_replace($error_attribute, '...', 50) : $error_attribute;
				$content = trim($content) == '' ? '(Message not found during import)' : $content;
				$error_attribute = '<span style="padding-top: 5px; font-weight: bold;">Error Message</span><br/><div class="alert alert-error">' . $error_attribute . '</div>';
				$a_el = new HtmlElement('a');
				$a_el->attr('data-content', $hints_form . substr_replace($error_attribute, '...', '500'))
					->attr('data-placement', 'right')
					->attr('title', 'Overview')
					->attr('href', url_for('details', $iris_error['error_id']))
					->attr('target', '_blank')
					->attr('class', 'messages')
					->addChild($content);
				$error_attribute = (string)$a_el;
				$error_attribute = $error_attribute . '<p style="color: #AAAAAA; font-style: italic; font-size: 8pt;"> admitted last <span style="color: #C9B09F;">' . _formatStringDate($iris_error['last_show'], TRUE, 'F j, Y') . '</span></p>';
			}
			else if ($error_key === 'trac_ticket') {
				if ($error_attribute) {
					$href = "https://trac.illuminateed.com/ticket/{$error_attribute}";
					$a_el = new HtmlElement('a');
					$a_el->attr('data-content', "Ticket #{$error_attribute}")
						->attr('href', $href)
						->attr('target', '_blank')
						->addChild("#{$error_attribute}");
					$el = new HtmlElement('div');
					$el->attr('style', 'text-align: center;')
						->addChild($a_el);
				}
				else {
					$el = new HtmlElement('button');
					$el->attr('class', 'btn')
						->attr('data-name', 'create')
						->attr('type', 'button')
						->attr('value', $iris_error['error_id'])
						->attr('class', 'create_ticket')
						->addChild('Create');
				}
				$error_attribute = (string)$el;
			}
			else if($error_key == 'tags') {
				continue;
			}

			$errors[$error_key] = $error_attribute;
		}
		$formatted_errors[$iris_error['error_id']] = $errors;
	}

	return $formatted_errors;
}

function _getErrorColumnHeaders()
{
	$display_columns = array(
		'count',
		'message',
		'last_show',
		'owner',
		'type',
		'status',
		'component',
		'trac_ticket',
		'update',
	);

	$error_headers = array();
	foreach ($display_columns as $display_column) {
		$error_headers[] = ucwords(str_replace('_', ' ', $display_column));
	}

	return $error_headers;
}

dispatch('/', 'index');
dispatch_post('update_filter', 'index');
function index()
{
	$progress_el = new HtmlElement('div');
	$progress_el->attr('class', 'bar')
		->attr('style', 'width: 100%');

	$div_el = new HtmlElement('div');
	$div_el->attr('class', 'progress')
		->attr('class', 'progress-striped active')
		->addChild($progress_el);
	set('loading', (string)$div_el);
	set('post', $_POST);
	return html('index.php');
}

dispatch('/error_list', 'error_list');
function error_list()
{
	$options = $_GET;
	$iris_errors = IrisError::getBugList($options);
	$formatted_errors = _formatErrorData($iris_errors);
	$error_headers = _getErrorColumnHeaders();
	$last_import = _formatStringDate(ImportLog::getLastImportDate(), TRUE);
	set('last_import', $last_import);
	set('errors', $formatted_errors);
	set('error_headers', $error_headers);
	set('use_saved_filter', isset($options['use_saved_filter']));
	set('selected_filters', (isset($options['option']) ? $options['option'] : NULL));
	set('selected_saved_filter', (isset($options['save_filter']) ? $options['save_filter'] : NULL));

	return partial('errors/error_list.php');
}

dispatch_put('/error_update', 'error_update');
function error_update()
{
	$status = array();
	if($_POST['action'] == 'Merge') {
		$error_count = $status['updated'] = 0;
		$merged_error = NULL;
		$merge_errors = $_POST['delete'];
		$last_error = NULL;

		if(count($merge_errors) > 1) {
			foreach ($_POST['delete'] as $key => $error_id) {
				$error = IrisError::findByErrorId($error_id);
				if ( ! $error) {
					continue;
				}
				$error_count += $error->count;
				if($error->trac_ticket) {
					$merged_error = $error;	
					unset($merge_errors[$key]);

				}
				$last_error = array($key, $error);
				$status['updated']++;
			}
			if(is_null($merged_error)) {
				$merged_error = $last_error[1]; 
				unset($merge_errors[$last_error[0]]);
			}
			$deleted = IrisError::deleteByIds($merge_errors);

			$merged_error->count = $error_count;
			$merged_error->save();
			$status['updated'] = "<span class=\"note\" style=\"color: #A0B572; margin-top: 3px;\">Merged {$status['updated']} Bugs </span><input type=\"hidden\" id=\"retained\" value=\"{$merged_error->error_id}\" count=\"{$error_count}\">";
		}
	}
	else {
		if (isset($_POST['delete'])) {
			$deleted = IrisError::deleteByIds($_POST['delete']);
			$status['deleted'] = "Deleted {$deleted} errors.";
		}

	}

	if (isset($_POST['update'])) {
		$status['updated'] = 0;
		foreach ($_POST['update'] as $error_id => $attr) {
			$error = IrisError::findByErrorId($error_id);
			if ( ! $error) {
				continue;
			}
			foreach ($attr as $attr_name => $attr_value) {
				$error->$attr_name = $attr_value;
			}
			$error->save();
			$status['updated']++;
		}
		$status['updated'] = "Updated {$status['updated']} errors.";
	}
	$class = $status ? 'alert-success' : 'alert-info';
	$message = $status ? implode('<br>', $status) : 'Nothing changed.';
	$div_el = new HtmlElement('div');
	$div_el->attr('class', "alert")
		->attr('class', $class)
		->addChild($message);

	set('status', (string)$div_el);
	return partial('errors/error_update.php');
}

dispatch('/details/:error_id', 'get_details');
function get_details() 
{
	$error = IrisError::findByErrorId(params('error_id'));
	set('details', $error->getDetails());
	return html('errors/details.php');
}

//Login page
dispatch('/login', 'AuthController::showLogin');
dispatch_post('/login', 'AuthController::showLogin');
dispatch('/logout', 'AuthController::logout');

//index page of Admin
dispatch('/admin/:section', 'AdminController::showSection');
dispatch_post('/update_filters', 'FilterController::updateFilters');
dispatch_post('/add_user', 'UserController::addUser');
dispatch_post('/update_users', 'UserController::updateUsers');

//index page for Updating account
dispatch('/account', 'AccountController::showAccount');
dispatch_post('/update_account', 'AccountController::updateAccount');

//ticket handling
dispatch_post('/create_ticket', 'Ticketcontroller::createTicket');

//statistics 
dispatch('/statistics', 'StatisticsController::showStat');

run();

?>
